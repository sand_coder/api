require 'rails_helper'

RSpec.describe AccessTokensController, type: :controller do
	describe "POST #create" do

		context "when no code is provided" do
			subject {post :create}
			it_behaves_like "unauthorized_requests"
		end

		context "when invalid code is provided" do
			subject {post :create, params: {code: "invalid"}}
			it_behaves_like "unauthorized_requests"
		end

		context "when success request" do
			let(:user_data) do
					{
						login: 'sandeep1',
						url: 'http://localhost:3000',
						avatar_url: 'http://localhost:3000/avatar',
						name: 'sandeep soni'
					}
			end
			before do
				allow_any_instance_of(Octokit::Client).to receive(
					:exchange_code_for_token).and_return('validaccesstoken')

				allow_any_instance_of(Octokit::Client).to receive(:user).and_return(user_data)
			end
			subject {post :create, params: {code: "valid_code"}}
			it "should return 201 status code" do
				subject
				expect(response).to have_http_status(:created)
			end

			it "shoul return proper json body" do
				expect{subject}.to change{User.count}.by(1)
				user = User.find_by(login: "sandeep1")
				token = user.access_token
				expect(json_data["attributes"]).to eq({'token' => user.access_token.token})
			end
		end
	end

	describe "DELETE #destroy" do
		subject {delete :destroy}

		context "When no authorization error is provided" do			
			it_behaves_like "forbidden_requests"
		end

		context "When invalid authorization error is provided" do			
			before {request.headers['authorization'] = "Invalid token"}
			it_behaves_like "forbidden_requests"
		end

		context "when request is valid" do
			let(:user) { create :user}
			let(:access_token){user.create_access_token}
			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

			it "should return 204 status code" do
				subject
				expect(response).to have_http_status(:no_content)
			end

			it "should remove the proper access token" do
				expect{subject}.to change{AccessToken.count}.by(-1)
			end
		end
	end
end
