require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
	describe "#index" do
		subject { get :index}
		it "Should return response" do
			subject
			expect(response).to have_http_status(:ok)
		end

		it "should return proper json" do
			articles = create_list :article, 2
			subject
			Article.recent.each_with_index do |article, index|
				expect(json_data[index]["attributes"]).to eq({
					"title" => article.title,
					"content" => article.content,
					"slug" => article.slug
				})
			end
		end

		it "should return articles in proper order" do
			# old_article = create :article
			# newer_article = create :article
			# subject
			# pp json_data
			# expect(json_data.first["id"]).to eq(newer_article.id.to_s)
			# expect(json_data.last["id"]).to eq(old_article.id.to_s)
		end

		it "Should paginate result" do
			create_list :article, 3
			get :index, params: {page: 2, per_page: 1}
			expected_article = Article.recent.second.id.to_s
			expect(json_data.length).to eq(1)

			expect(json_data.first["id"]).to eq(expected_article)
		end
 	end

 	describe "#show" do
 		let(:article) {create :article}
 		subject {get :show, params: {id: article.id} }

 		it "Should return success response" do
 			subject
 			expect(response).to have_http_status(:ok)
 		end

 		it "should return proper json" do
 			subject
 			expect(json_data["attributes"]).to eq({
 				"title" => article.title,
 				"content" => article.content,
 				"slug" => article.slug
 			})
 		end
 	end	


 	describe "POST #create" do
 		subject {post :create}
 		context "when no code is provided" do
 			it_behaves_like "forbidden_requests"
 		end

 		context "when invalid code is provided" do
 			before {request.headers["authorization"] = "Invalid token"}
 			it_behaves_like "forbidden_requests"
 		end
 		context "when Authorized" do
 			let(:user) { create :user}
			let(:access_token){user.create_access_token}
			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

 			context "when in valid params is provided" do
 				let(:invalid_params) do
 					{
 						data:{
 							attributes: {
 								title: '',
 								content: ''
 							}
 						}
 					}
 				end
 				subject{post :create,params: invalid_params }
 				it "should return status code 422" do
 					subject
 					expect(response).to have_http_status(:unprocessable_entity)
 				end

 				it "should return proper error json" do
 					subject
 					expect(json["errors"]).to include(
 					{				      
				      "source" => { "pointer" => "/data/attributes/title" },
				      "detail" => "can't be blank"
				   },{				      
				      "source" => { "pointer" => "/data/attributes/content" },
				      "detail" => "can't be blank"
				   },{				      
				      "source" => { "pointer" => "/data/attributes/slug" },
				      "detail" => "can't be blank"
				   }
				   )
 				end
 			end
	 	end

	 	context "when success request sent" do
	 		let(:user) { create :user}
			let(:access_token){user.create_access_token}
			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

 			context "when in valid params is provided" do
				let(:valid_params) do
					{
						"data" =>{
							"attributes" => {
								"title" => 'my article',
								"content" => 'my article content',
								"slug" => "my-article"
							}
						}
					}
				end
 				subject {post :create,params: valid_params }

 				it "should return 401 status code" do
 					subject
 					expect(response).to have_http_status(201)
 				end

 				it "should return proper json data" do
 					subject
 					expect(json_data["attributes"]).to include(valid_params["data"]["attributes"])
 				end

 				it "should article count increase by 1" do
 					expect{ subject }.to change{Article.count}.by(1)
 				end
 			end
	 	end
 	end

 	describe "PUT #update" do
 		let(:user) { create :user}
 		let(:access_token){user.create_access_token}
 		let(:article) {create :article, user: user}
 		subject {put :update,params: {id: article.id } }
 		context "when no code is provided" do
 			it_behaves_like "forbidden_requests"
 		end

 		context "when invalid code is provided" do
 			before {request.headers["authorization"] = "Invalid token"}
 			it_behaves_like "forbidden_requests"
 		end

 		context "when Authorized" do
			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

 			context "when in valid params is provided" do
 				let(:invalid_params) do
 					{
 						"data" => {
 							"attributes" => {
 								"title" => '',
 								"content" => ''
 							}
 						}
 					}
 				end
 				subject {patch :update,params: invalid_params.merge(id: article.id) }
 				it "should return status code 422" do
 					subject
 					expect(response).to have_http_status(:unprocessable_entity)
 				end

 				it "should return proper error json" do
 					subject
 					expect(json["errors"]).to include(
 					{				      
				      "source" => { "pointer" => "/data/attributes/title" },
				      "detail" => "can't be blank"
				   },{				      
				      "source" => { "pointer" => "/data/attributes/content" },
				      "detail" => "can't be blank"
				   }
				   )
 				end
 			end
	 	end

	 	context "when trying to update not owned articles" do
	 		let(:other_user) { create :user }
	 		let(:other_article) {create :article, user: other_user}
	 		subject {patch :update,params: {id: other_article.id} }
	 		before {request.headers['authorization'] = "Bearer #{access_token.token}"}
	 		it_behaves_like "forbidden_requests"
	 	end

	 	context "when success request sent" do
			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

 			context "when in valid params is provided" do
				let(:valid_params) do
					{
						"data" =>{
							"attributes" => {
								"title" => 'my article',
								"content" => 'my article content',
								"slug" => "my-article"
							}
						}
					}
				end
 				subject {patch :update,params: valid_params.merge(id: article.id) }

 				it "should return 201 status code" do
 					subject
 					expect(response).to have_http_status(:ok)
 				end

 				it "should return proper json data" do
 					subject
 					expect(json_data["attributes"]).to include(valid_params["data"]["attributes"])
 				end

 				it "should article count increase by 1" do
 					expect{ subject }.to change{Article.count}.by(1)
 				end

 				it "should update article" do
 					subject
 					expect(article.reload.title).to eq(valid_params["data"]["attributes"]["title"])
 				end
 			end
	 	end
 	end

 	describe "DELETE #destroy" do
 		let(:user) { create :user}
 		let(:access_token){user.create_access_token}
 		let(:article) {create :article, user: user}

 		subject {delete :destroy, params: {id: article.id } }

 		context "when no code is provided" do
 			it_behaves_like "forbidden_requests"
 		end

 		context "when invalid code is provided" do
 			before {request.headers["authorization"] = "Invalid token"}
 			it_behaves_like "forbidden_requests"
 		end

 		context "when trying to delete not owned articles" do
	 		let(:other_user) { create :user }
	 		let(:other_article) {create :article, user: other_user}

	 		subject {delete :destroy,params: {id: other_article.id} }

	 		before {request.headers['authorization'] = "Bearer #{access_token.token}"}
	 		it_behaves_like "forbidden_requests"
	 	end

	 	context "When Authorized" do
	 		before {request.headers['authorization'] = "Bearer #{access_token.token}"}
	 		context "when success request sent" do
	 			before {request.headers['authorization'] = "Bearer #{access_token.token}"}

	 			it "should return 204 status code" do
					subject
					expect(response).to have_http_status(:no_content)
				end

				it "should have empty json body" do
					subject
					expect(response.body).to be_blank
				end 

				it "should remove the article" do
					article
					expect{subject}.to change{user.articles.count}.by(-1)
				end
	 		end
	 	end
 	end
end