FactoryBot.define do
  factory :comment do
    sequence(:content) {|n| "My article" }
    association :article
    association :user
  end
end
