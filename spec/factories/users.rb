FactoryBot.define do
  factory :user do
    sequence(:login) {|n| "Sandeep #{n}" }
    name { "Sandeep Soni" }
    url { "http://localhost:3000" }
    avatar_url { "http://localhost:3000/avatar" }
    provider { "Github" }
  end
end
