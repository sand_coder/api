require 'rails_helper'

RSpec.describe User, type: :model do
  describe "Validations" do
  	subject { User.create(login: "Here is the content", provider: "Github") }
  	it "Should have valida factory" do
  		user = build :user
  		expect(user).to be_valid
  	end
  	it "Should validate presence of attributes" do
  		user = build :user, login: nil, provider: nil
  		expect(user).not_to be_valid
  		expect(user.errors.messages[:login]).to include("can't be blank")
  		expect(user.errors.messages[:provider]).to include("can't be blank")
  	end

  	it { should validate_uniqueness_of(:login) }

  	# it "Should validate uniqueness of login" do
  	# 	user = build :user
  	# 	pp user.login
  	# 	other_user = build :user, login: user.login
  	# 	expect(other_user).not_to be_valid
  	# 	other_user.login = "newLogin"
  	# 	expect(other_user).to be_valid
  	# end
  end
end
