require 'rails_helper'

describe "Articles routes" do
	it "Should route to articles index" do
		expect(get '/articles').to route_to('articles#index')
	end

	it "Should route to articles index" do
		expect(get '/articles/1').to route_to('articles#show',id: '1')
	end

	it "Should route to articles create" do
		expect(post '/articles').to route_to('articles#create')
	end

	it "Should route to articles update" do
		expect(put 'articles/1').to route_to('articles#update',id: '1')
		expect(patch 'articles/1').to route_to('articles#update',id: '1')
	end

	it "should route to articles  destroy action" do
		expect(delete 'articles/1').to route_to('articles#destroy',id: '1')
	end
end